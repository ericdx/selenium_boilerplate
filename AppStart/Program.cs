﻿using OpenQA.Selenium.Chrome;
using ParseLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace AppStart
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = new ChromeOptions();
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("--ignore-ssl-errors");
            var web = new Parser(Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().Location),options);
            var base_ = "https://krsk.au.ru";
            // web.Manage().Timeouts().PageLoad = TimeSpan.FromMinutes(55);
            var cat = "/auction/phones/smartphony/";
            
            CatModel mdl = web.parseCategory(base_,cat, "au-lot-list-card-title-link", new string[] {
                "au-lot-parameter", "au-lot-parameter-title", "au-lot-parameter-value" });
          
File.WriteAllLines("out.csv",mdl.toCSV());
            //File.WriteAllText("avito.htm", web.PageSource);
            web.Quit();
        }
    }
}
