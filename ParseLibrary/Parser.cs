﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using ParseLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TextLib;

namespace AppStart
{
    public class Parser : ChromeDriver
    {
        static Parser() {
            
        }
        public Parser(string chromeDriverDirectory,ChromeOptions opt) 
            : base(chromeDriverDirectory,opt)
        {
            //  Manage().Timeouts(). = TimeSpan.FromMinutes(55);
            //var options = new ChromeOptions();
            //options.AddArgument("--ignore-certificate-errors");
            //options.AddArgument("--ignore-ssl-errors");
            
        }

        public List<string> classesText(string class_)
        {
            var list = new List<string>();
            foreach (var el in FindElementsByClassName(class_)) {
                list.Add(el.Text);
            }
            return list;
        }

        public CatModel parseCategory(string base_, string catUrl, string itemLink, string[] itemProp)
        {
            int i = 1;
            var items = new List<PageItem>();
            do
            {
                Navigate().GoToUrl(base_ + $"{catUrl}?page={i}");
                items.AddRange(scanPage(itemLink, itemProp));
            }
            while (i++ <= 2);
            return new CatModel(base_, items);
        }

        public List<PageItem> scanPage(string class_, string[] itemProp)
        {
            var list = new List<PageItem>();
            
            
                foreach (var el in FindElementsByClassName(class_).Take(1))
                {
                    var href = el.GetAttribute("href");
                var descr = el.Text;
                    Navigate().GoToUrl(href);
                    list.Add(new PageItem { text = descr, link = href, props = getProps(itemProp) });
                    Navigate().Back();
                } 
            return list;
        }

        private List<Prop> getProps(string[] itemProp)
        {
            var props = new List<Prop>();
            FindElementsByClassName(itemProp[0]).ToList().ForEach(div =>
            {
               var titles = div.FindElements(By.ClassName(itemProp[1])).Select(pt => pt.Text).ToArray();
                var i = 0;
                div.FindElements(By.ClassName(itemProp[2])).ToList().ForEach(val =>
                {
                    props.Add(new Prop(titles[i++], val.Text));
                });
            });
            return props;
        }

        public void auth(string login, string pwd, string btn)
        {
            string cred;
            Settings.fromFile("auth", out cred);
            var auth = cred.Split(';');
            FindElementById(login).SendKeys(auth[0]);
            FindElementById(pwd).SendKeys(auth[1]);
            FindElementById(btn).Click();
        }

        public void click(string[] attrs,string vl) {
            FindElementsByTagName(attrs[0]).First(a => a.GetAttribute(attrs[1]) == vl)
                    .Click();
        }
        public List<PageItem> follow_pages(string class_, string linkAttr,string textClass)
        {
              var list = new List<PageItem>();
            foreach (var el in FindElementsByClassName(class_)) {
                var link = el.GetAttribute(linkAttr);
                list.Add(new PageItem(el.Text,link));
            }
            foreach (var el in list)//.Take(2))
            {
                base.Navigate().GoToUrl( el.link);
                try
                {
                    var block = FindElementByClassName(textClass);

                    foreach (var p in block.FindElements(By.TagName("p")))
                    {
                        el.text += p.Text + "\t";
                    }
                }
                catch (NoSuchElementException)
                {
                    el.text = "no description";
                }
            }
            return list;
        }
    }
}
