﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppStart
{
    public class PageItem
    {
        public string text = "";

        public string caption { get; private set; }

        public string link;
        public string line => $"{link};{text}";

        public List<Prop> props { get; internal set; }

        public PageItem(string _caption, string link)
        {
            caption = _caption;
            this.link = link;
        }
        public PageItem() { }
    }

    public class Prop
    {
        private string title;
        private string text;

        public Prop(string title, string text)
        {
            this.title = title;
            this.text = text;
        }
        public override string ToString() {
            return $"{title};{text}";
        }
    }
}
