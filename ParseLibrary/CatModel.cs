﻿using AppStart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParseLibrary
{
    public class CatModel
    {
        public string catName;
        public string site; //au
        public List<PageItem> items;
       

        public CatModel(string base_, List<PageItem> list)
        {
            this.site = base_;
            this.items = list;
        }

        public string[] toCSV()
        {            
           return items.SelectMany(i => i.props).Select(p => p.ToString()).ToArray();
        }
    }
}
